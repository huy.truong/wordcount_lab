package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class Pair extends Configured implements Tool {

  public static class PairMapper 
   extends Mapper<LongWritable, // TODO: change Object to input key type
                  Text, // TODO: change Object to input value type
                  TextPair, // TODO: change Object to output key type
                  IntWritable> { // TODO: change Object to output value type
    // TODO: implement mapper
    protected void map(LongWritable key, Text text, Context context)  {
        //TODO :
        TextPair tp = new TextPair();
        //context.write(tp, IntWritable(1));
    }

  }

  public static class PairReducer
    extends Reducer<TextPair, // TODO: change Object to input key type
                    IntWritable, // TODO: change Object to input value type
                    TextPair, // TODO: change Object to output key type
                    IntWritable> { // TODO: change Object to output value type

     IntWritable writableSum= new IntWritable();
    // TODO: implement reducer
    protected void reduce(TextPair tp,
                          Iterable<IntWritable> values,
                          Context context) throws IOException, InterruptedException {

        // TODO: implement the reduce method (use context.write to emit results)
        int sum = 0;
        for (IntWritable value : values)
            sum += value.get();

        writableSum.set(sum);
        context.write(tp,writableSum);
    }
  }

  private int numReducers;
  private Path inputPath;
  private Path outputDir;

  public Pair(String[] args) {
    if (args.length != 3) {
      System.out.println("Usage: Pair <num_reducers> <input_path> <output_path>");
      System.exit(0);
    }
    this.numReducers = Integer.parseInt(args[0]);
    this.inputPath = new Path(args[1]);
    this.outputDir = new Path(args[2]);
  }
  

  @Override
  public int run(String[] args) throws Exception {

      // TODO: set job input format
      Configuration conf = this.getConf();

      Job job = new Job(conf,"Word Count");

      job.setInputFormatClass(TextInputFormat.class);

      // TODO: set map class and the map output key and value classes
      job.setMapperClass(PairMapper.class);
      job.setMapOutputKeyClass(Text.class);
      job.setMapOutputValueClass(IntWritable.class);

      // TODO: set reduce class and the reduce output key and value classes
      job.setReducerClass(PairReducer.class);
      job.setOutputKeyClass(Text.class);
      job.setOutputValueClass(IntWritable.class);

      // TODO: set job output format
      job.setOutputFormatClass(TextOutputFormat.class);

      // TODO: add the input file as job input (from HDFS) to the variable inputFile
      FileInputFormat.addInputPath(job,this.inputPath);
      // TODO: set the output path for the job results (to HDFS) to the variable outputPath
      FileOutputFormat.setOutputPath(job, this.outputDir);

      // TODO: set the number of reducers using variable numberReducers
      job.setNumReduceTasks(this.numReducers);

      // TODO: set the jar class

      job.setJarByClass(WordCount.class);

      job.waitForCompletion(true);



    return job.waitForCompletion(true) ? 0 : 1;
  }

  public static void main(String[] args) throws Exception {
    int res = ToolRunner.run(new Configuration(), new Pair(args), args);
    System.exit(res);
  }
}
